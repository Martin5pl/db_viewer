import sqlalchemy

from DbAgent import DbAgent

params = {'name': 'sdsi_masterdata',
          'url': 'localhost:5432',
          'user': 'db_user',
          'password': 'db_password'}

params2 = {'name': 'sdsi_dataprocessor',
           'url': 'localhost:5433',
           'user': 'db_user',
           'password': 'db_password'}

try:
    agent = DbAgent(params2)
except sqlalchemy.exc.OperationalError as e:
    print('connection error')
    print(e)
    exit(1)


print(agent.all_table_names)
print(agent.tables_count)
