from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


class DbAgent:
    def __init__(self, db_params):
        db_string = self._create_db_string(db_params)
        db = create_engine(db_string)
        # noinspection PyPep8Naming
        Session = sessionmaker(db)
        self._session = Session()
        self._db_base = declarative_base()
        # self.db_base.metadata.reflect(bind=db)

        self.all_table_names = db.table_names()
        self.tables_count = len(self.all_table_names)

    def _create_db_string(self, db_params):
        result = 'postgres://{}:{}@{}/{}'.format(db_params['user'], db_params['password'],
                                                 db_params['url'], db_params['name'])

        return result

    def _get_table(self, table_name):
        return self._db_base.metadata.tables.get(table_name)

    def _get_table_columns(self, table_name):
        table = self._get_table(table_name)
        result = [t.name for t in table.columns]
        return result

    # def clear_all_tables(self):
    #     for table_name in self.all_table_names:
    #         table = self.db_base.metadata.tables.get(table_name)
    #         self.session.execute(f'ALTER TABLE "{table.name}" DISABLE TRIGGER ALL;')
    #         self.session.execute(table.delete())
    #         self.session.execute(f'ALTER TABLE "{table.name}" ENABLE TRIGGER ALL;')
    #     self.session.commit()

    # returns always a list of entities
    def read_all_from_table(self, table, **criteria):
        return self._session.query(table).filter_by(**criteria).all()

    def count_records_in_table(self, table):
        return self._session.query(table).count()
