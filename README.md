### Database client with GUI
Initial version of a program

**Required components:**  

  * PyQt5
  
  * PyQt5Designer
  
  * requests

**To install plugins:**  
```pip3 install -r requirements.txt```


**To start:**  
```python main.py```
 
