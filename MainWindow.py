from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QMessageBox

from gui import mainWindow
from tools import empty_values_in_dict, find_db_index_in_configuration


class MainWindow(QMainWindow, mainWindow.Ui_MainWindow):

    def __init__(self, configuration, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.configuration = configuration
        self.message_dialog = QMessageBox()
        self.update_actual_state()
        self.update_edit_fields()

        self.button_add.clicked.connect(self.add_db_to_config)
        self.button_remove.clicked.connect(self.remove_db_from_config)
        self.comboBox_db_config.currentIndexChanged.connect(self.update_edit_fields)

    def add_db_to_config(self):
        db_params = {'name': self.edit_db_name.text(),
                     'url': self.edit_url.text(),
                     'user': self.edit_user.text(),
                     'password': self.edit_password.text()}

        if empty_values_in_dict(db_params):
            self.message_dialog.about(self, 'Error', 'No field should be empty.')
            return

        self.update_configuration(db_params)
        self.update_actual_state()

    def remove_db_from_config(self):
        db_name = {'name': self.edit_db_name.text()}
        self.update_configuration(db_name, remove=True)
        self.update_actual_state()

    def update_edit_fields(self):
        db_name = self.comboBox_db_config.currentText()
        db_params = {'name': '',
                     'url': '',
                     'user': '',
                     'password': ''}
        index = find_db_index_in_configuration(db_name, self.configuration)
        if index >= 0:
            db_params = self.configuration['databases'][index]

        self.edit_db_name.setText(db_params['name'])
        self.edit_url.setText(db_params['url'])
        self.edit_user.setText(db_params['user'])
        self.edit_password.setText(db_params['password'])

    def update_configuration(self, db_params, remove=False):
        db_name = db_params['name']
        index = find_db_index_in_configuration(db_name, self.configuration)
        if index >= 0:
            if remove:
                _ = self.configuration['databases'].pop(index)
                self.statusbar.showMessage(f'Removed parameters of database named {db_name}.')
            else:
                self.configuration['databases'][index] = db_params
                self.statusbar.showMessage(f'Modified parameters of existing database named {db_name}.')
            return
        elif not remove:
            self.configuration['databases'].append(db_params)
            self.statusbar.showMessage(f'Added parameters of new database named {db_name}.')

    def update_actual_state(self):
        self.comboBox_db_connect.clear()
        self.comboBox_db_config.clear()
        self.comboBox_db_config.addItem('New database params')
        if len(self.configuration['databases']) == 0:
            self.comboBox_db_connect.addItem('No databases configured')
            self.comboBox_db_config.setCurrentIndex(0)
            self.configuration['selected_db'] = None
            return

        for dbs in self.configuration['databases']:
            self.comboBox_db_config.addItem(dbs['name'])
            self.comboBox_db_connect.addItem(dbs['name'])

        items = self.comboBox_db_config.count()
        self.comboBox_db_config.setCurrentIndex(items-1)
        # if self.configuration['selected_db'] is None:
        #     db_name = self.comboBox_db_config.currentText()
        #     self.configuration['selected_db'] = db_name
        #     index = self.comboBox_db_connect.findText(db_name)
        #     self.comboBox_db_connect.setCurrentIndex(index)
