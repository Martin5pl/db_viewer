def empty_values_in_dict(tested_dict):
    for _, val in tested_dict.items():
        if val == '' or val is None:
            return True
    return False


def find_db_index_in_configuration(db_name, configuration):
    for index, dic in enumerate(configuration['databases']):
        if dic['name'] == db_name:
            return index
    return -1
