from PyQt5.QtWidgets import *

import sys
import pickle

from MainWindow import MainWindow

CONFIG_FILE = 'config.cfg'
configuration = {'databases': [], 'selected_db': None}


def load_configuration_from_file():
    global configuration
    try:
        with open(CONFIG_FILE, 'rb') as f:
            configuration = pickle.load(f)
    except FileNotFoundError:
        pass


def save_configuration_to_file():
    with open(CONFIG_FILE, 'wb') as f:
        pickle.dump(configuration, f)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    load_configuration_from_file()
    form = MainWindow(configuration)
    form.show()
    app.exec_()
    save_configuration_to_file()
